package com.mcmxc;

import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MyFileUploadController {


    @RequestMapping(value = "/upload", method = RequestMethod.POST/*, consumes = MediaType.MULTIPART_FORM_DATA_VALUE*/)
    public byte[]/*ResponseEntity<MultipartFile>*/ uploadFile(@RequestParam("first") MultipartFile first,
                                                            @RequestParam("second") MultipartFile second,
                                                            @RequestParam("contentWeight") BigDecimal contentWeight,
                                                            @RequestParam("styleWeight") BigDecimal styleWeight,
                                                            @RequestParam("styleScale") Long styleScale,
                                                            @RequestParam("originalColors") boolean originalColors
    ) throws IOException {



        File input = createTempDirectory("neural");
        File output = new File(input.getAbsolutePath() + File.separator + "output");
        output.mkdir();

        MultipartFile[] fileDatas = new MultipartFile[2];
        fileDatas[0] = first;
        fileDatas[1] = second;

        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();


        for (MultipartFile fileData : fileDatas) {

            String name = fileData.getOriginalFilename();
            System.out.println("Client File Name = " + name);

            if (name != null && name.length() > 0) {
                try {
                    File serverFile = new File(input.getAbsolutePath() + File.separator + name);
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    uploadedFiles.add(serverFile);
                    System.out.println("Write file: " + serverFile);

                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                    failedFiles.add(name);
                }
            }
        }

        ImageConverter imageConverter;

        DirectoryWatcher directoryWatcher = new DirectoryWatcher(output.getAbsolutePath());
        if (contentWeight == null || styleScale == null || styleWeight == null) {
            imageConverter = new ImageConverterImpl(fileDatas[0].getBytes(), fileDatas[1].getBytes(), originalColors);
        } else {
            imageConverter = new ImageConverterImpl(fileDatas[0].getBytes(), fileDatas[1].getBytes(), contentWeight,
                    styleWeight, styleScale, originalColors);
        }


        byte[] result =imageConverter.neuralImageConvert();

        File outputFile = new File(output.getAbsolutePath() + File.separator + "result.bmp");
        FileUtils.writeByteArrayToFile(outputFile, imageConverter.neuralImageConvert());

        for (; ; ) {
            if (directoryWatcher.isResultReady == true) {
                System.out.println("ready");
                FileUtils.deleteDirectory(input);
                return result;
            } else {
                try {
                    Thread.sleep(1000);
                    System.out.println("wait");
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }


    public static File createTempDirectory(String prefix) {
        final File tmp = new File(FileUtils.
                getTempDirectory().
                getAbsolutePath() +
                "/" + prefix);
        tmp.mkdir();
        return tmp;
    }
}
