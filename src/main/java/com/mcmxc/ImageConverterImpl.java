package com.mcmxc;

import java.math.BigDecimal;

public class ImageConverterImpl implements ImageConverter {

    private boolean originalColors;
    private BigDecimal contentWeight;
    private BigDecimal styleWeight;
    private Long styleScale;
    private byte[] first;
    private byte[] second;


    public ImageConverterImpl(byte[] first, byte[] second, BigDecimal contentWeight, BigDecimal styleWeight, Long styleScale, boolean originalColors) {
        this.first = first;
        this.second = second;
        this.contentWeight = contentWeight;
        this.styleScale = styleScale;
        this.originalColors = originalColors;
        this.styleWeight = styleWeight;
    }

    public ImageConverterImpl(byte[] first, byte[] second, boolean originalColors) {
        this.first = first;
        this.second = second;
        this.contentWeight = new BigDecimal(5);
        this.styleWeight = new BigDecimal(100);
        this.styleScale = 1l;
        this.originalColors = originalColors;
    }

    @Override
    public byte[] neuralImageConvert() {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return first;
    }


}
